#!/usr/bin/env python3
# Adélie Linux architecture package tester
# Ensure all packages are all on arches
#
# Copyright © 2018 Adélie Linux team.  All rights reserved.
# NCSA license.
#

from apkkit.base.index import Index

BASE_URL = "https://mirrormaster.adelielinux.org/adelie/{version}/{repo}/{arch}/APKINDEX.tar.gz"
"""The base URL to use for downloading package indexes."""

VERSION = "1.0-alpha6"
"""The version to check.  Bump this after each release."""

def compare_versions(old, new):
    """Compare two versions.  Return True if equal."""
    return (old == new)

def compare_older(old, new):
    """Compare two versions.  Return True if old is older."""
    for part in zip(old.split('.'), new.split('.')):
        # part[0] = next component of old ver, part[1] = new
        try:
            oldpart = int(part[0])
            newpart = int(part[1])
        except ValueError:
            oldpart = part[0]
            newpart = part[1]
        if oldpart < newpart:
            return True
        elif oldpart == newpart:
            continue
        else:
            return False
    return False

def process_one_pkg(mydict, next_pkg):
    """Process a package."""
    new = next_pkg.version
    if next_pkg.origin in mydict:
        curr = mydict[next_pkg.origin]

        if not compare_versions(curr, new) and compare_older(curr, new):
            mydict[next_pkg.origin] = new
        else:
            pass  # Do nothing if this is older.
    else:  # Doesn't exist in this dict, put it in.
        mydict[next_pkg.origin] = new

def main():
    """The main function."""

    arches = set()
    sys_ign = set()
    user_ign = set()
    with open('arches', 'r') as arch_file:
        arches = [arch[:-1] for arch in arch_file.readlines()]
    with open('sys-specific', 'r') as ignore_file:
        sys_ign = set(pkg[:-1] for pkg in ignore_file.readlines())
    with open('user-specific', 'r') as ignore_file:
        user_ign = set(pkg[:-1] for pkg in ignore_file.readlines())

    all_sys_pkgs = dict()
    all_user_pkgs = dict()
    pkgs = {}

    arch_sys_pkgs = {arch: dict() for arch in arches}
    arch_user_pkgs = {arch: dict() for arch in arches}
    broken_sys = list()
    broken_user = list()
    not_broken = list()

    for arch in arches:
        print("Loading " + arch + "...")
        arch_sys = Index(url=BASE_URL.format(version=VERSION, repo='system', arch=arch))
        arch_user = Index(url=BASE_URL.format(version=VERSION, repo='user', arch=arch))

        for next_pkg in arch_sys.packages:
            for mydict in (all_sys_pkgs, arch_sys_pkgs[arch]):
                process_one_pkg(mydict, next_pkg)

        for next_pkg in arch_user.packages:
            for mydict in (all_user_pkgs, arch_user_pkgs[arch]):
                process_one_pkg(mydict, next_pkg)

    all_sys_pkgs.pop('firefox-esr')
    all_sys_pkgs.pop('gcc')

    for arch in arches:
        old_sys = list()
        old_user = list()

        for pkg in all_sys_pkgs.keys():
            sysver = all_sys_pkgs[pkg]
            if pkg in arch_sys_pkgs[arch]:
                archver = arch_sys_pkgs[arch][pkg]
                if compare_older(archver, sysver):
                    old_sys.append((pkg, sysver, archver))

        for pkg in all_user_pkgs.keys():
            userver = all_user_pkgs[pkg]
            if pkg in arch_user_pkgs[arch]:
                archver = arch_user_pkgs[arch][pkg]
                if compare_older(archver, userver):
                    old_user.append((pkg, userver, archver))

        if len(old_sys) > 0:
            print("Outdated in {arch}/system:".format(arch=arch))
            print("=========================")
            old_sys.sort()
            for pkg, sys, archver in old_sys:
                print("{0}: {1} is older than {2}".format(pkg, archver, sys))
            print("\n\n")
            broken_sys.append(arch)

        if len(old_user) > 0:
            print("Outdated in {arch}/user:".format(arch=arch))
            print("=========================")
            old_user.sort()
            for pkg, user, archver in old_user:
                print("{0}: {1} is older than {2}".format(pkg, archver, user))
            print("\n\n")
            broken_user.append(arch)

        if len(old_sys) == 0 and len(old_user) == 0:
            not_broken.append(arch)

    print("\033[1;32mNo issues\033[1;0m: {a}".format(a=", ".join(not_broken)))
    print("\033[1;33mOutdated user\033[1;0m: {a}".format(a=", ".join(
        ["{x}".format(x=arch) for arch in broken_user])))
    print("\033[1;31mOutdated system\033[1;0m: {a}".format(a=", ".join(
        ["{x}".format(x=arch) for arch in broken_sys])))

if __name__ == "__main__":
    main()

