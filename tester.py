#!/usr/bin/env python3
# Adélie Linux architecture package tester
# Ensure all packages are all on arches
#
# Copyright © 2018 Adélie Linux team.  All rights reserved.
# NCSA license.
#

from apkkit.base.index import Index

BASE_URL = "https://mirrormaster.adelielinux.org/adelie/{version}/{repo}/{arch}/APKINDEX.tar.gz"
"""The base URL to use for downloading package indexes."""

VERSION = "1.0-alpha6"
"""The version to check.  Bump this after each release."""

def main():
    """The main function."""

    arches = set()
    sys_ign = set()
    user_ign = set()
    with open('arches', 'r') as arch_file:
        arches = [arch[:-1] for arch in arch_file.readlines()]
    with open('sys-specific', 'r') as ignore_file:
        sys_ign = set(pkg[:-1] for pkg in ignore_file.readlines())
    with open('user-specific', 'r') as ignore_file:
        user_ign = set(pkg[:-1] for pkg in ignore_file.readlines())

    all_sys_pkgs = set()
    all_user_pkgs = set()
    pkgs = {}

    broken_sys = list()
    broken_user = list()
    not_broken = list()

    for arch in arches:
        print("Loading " + arch + "...")
        arch_sys = Index(url=BASE_URL.format(version=VERSION, repo='system', arch=arch))
        arch_user = Index(url=BASE_URL.format(version=VERSION, repo='user', arch=arch))

        all_sys_pkgs = all_sys_pkgs.union(arch_sys.origins)
        all_user_pkgs = all_user_pkgs.union(arch_user.origins)

        pkgs[arch] = {'system': arch_sys.origins, 'user': arch_user.origins}

    for pkg in sys_ign:
        all_sys_pkgs.discard(pkg)
    for pkg in user_ign:
        all_user_pkgs.discard(pkg)

    for arch in arches:
        missing_sys = all_sys_pkgs - pkgs[arch]['system']
        missing_user = all_user_pkgs - pkgs[arch]['user']

        if len(missing_sys) > 0:
            print("Missing in {arch}/system:".format(arch=arch))
            print("=========================")
            missing = list(missing_sys)
            missing.sort()
            for pkg in missing: print(pkg)
            print("\n\n")
            broken_sys.append(arch)

        if len(missing_user) > 0:
            print("Missing in {arch}/user:".format(arch=arch))
            print("=========================")
            missing = list(missing_user)
            missing.sort()
            for pkg in missing: print(pkg)
            print("\n\n")
            broken_user.append(arch)

        if len(missing_sys) == 0 and len(missing_user) == 0:
            not_broken.append(arch)

    print("\033[1;32mNo issues\033[1;0m: {a}".format(a=", ".join(not_broken)))
    print("\033[1;33mMissing user\033[1;0m: {a}".format(a=", ".join(
        ["{x} ({y})".format(x=arch, y=len(all_user_pkgs - pkgs[arch]['user']))
            for arch in broken_user])))
    print("\033[1;31mMissing system\033[1;0m: {a}".format(a=", ".join(
        ["{x} ({y})".format(x=arch, y=len(all_sys_pkgs - pkgs[arch]['system']))
            for arch in broken_sys])))

if __name__ == "__main__":
    main()

